//QTouchMessenger: QML based message app
//Copyright (C) 2024  Dr. Tej A. Shah

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QSettings>
#include <QColor>
#include <QHostInfo>
#include <QPalette>
#include <QDebug>

#include "backend/messagemanager.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QGuiApplication app(argc, argv);
    app.setOrganizationName("Clear.Dental");
    app.setOrganizationDomain("clear.dental");
    app.setApplicationName("QTouchMessenger");

#if defined(Q_OS_LINUX)
    //qDebug()<<QGuiApplication::palette().color(QPalette::Active,QPalette::Highlight).name().toLocal8Bit();
    qputenv("QT_QUICK_CONTROLS_MATERIAL_ACCENT",
            QGuiApplication::palette().color(QPalette::Active,QPalette::Highlight).name().toLocal8Bit());
#endif

    QSettings defaultSettingsStuff;
    if(!defaultSettingsStuff.contains("hostName")) {
        defaultSettingsStuff.setValue("hostName", QHostInfo::localHostName());
    }

#if defined(Q_OS_MACOS)
    QQuickStyle::setStyle("macOS");
#elif defined(Q_OS_IOS)
    QQuickStyle::setStyle("iOS");
#elif defined(Q_OS_WINDOWS)
    QQuickStyle::setStyle("Windows");
#else
    QQuickStyle::setStyle("Material");
#endif

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/qml/QTouchMain.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);

    return app.exec();
}

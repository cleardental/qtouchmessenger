//QTouchMessenger: QML based message app
//Copyright (C) 2024  Dr. Tej A. Shah

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MESSAGEMANAGER_H
#define MESSAGEMANAGER_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include <QHostAddress>
#include <QQmlEngine>

class MessageManager : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QStringList hostNames READ getHostNames NOTIFY hostNamesUpdated)

public:
    explicit MessageManager(QObject *parent = nullptr);
    ~MessageManager();

    Q_INVOKABLE void sendCustomMessage(QString getMessage, QString getWhom);
    Q_INVOKABLE void sendGreeting();
    Q_INVOKABLE void sendHelpRequest(QString toWhom);
    Q_INVOKABLE QStringList getHostNames();

public slots:
    void readMessage();

signals:
     void hostNamesUpdated();
     void gotNewMessage(QString newMessage, QString fromWho);

private:
    QUdpSocket m_udpSocket4Send;
    QUdpSocket m_udpSocket4Rec;
    QUdpSocket m_udpSocket6Send;
    QUdpSocket m_udpSocket6Rec;
    QHostAddress m_groupAddress4;
    QHostAddress m_groupAddress6;
    QTimer *m_Timer;
    QStringList m_HostNames;

    void processUDPData(QByteArray getData);
    void sendRawMessage(QString getMessage);

};

#endif // MESSAGEMANAGER_H

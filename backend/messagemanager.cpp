//QTouchMessenger: QML based message app
//Copyright (C) 2024  Dr. Tej A. Shah

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "messagemanager.h"

#include <QDebug>
#include <QSettings>

#define UDP_PORT 45454

#define EVERYBODY_STRING tr("Everybody")
#define GREETING tr("Hello")
#define FAREWELL tr("Goodbye")
#define NEED_HELP tr("I need help")
#define DEST_SEPERATOR tr(": ")
#define END_MESSAGE_SEPERATOR tr(". | From: ")

//Format: [Destination]: [GREETING/FAREWELL/NEED_HELP/Custom]. | From: [Source]
//Example: "Everybody: Hello. | From: Setzer"
//Example: "Tifa: I need help. | From: Cloud"
//Example: "Edward: You spoony bard!. | From: Tellah"

MessageManager::MessageManager(QObject *parent)
    : QObject(parent),
      m_groupAddress4(QStringLiteral("239.255.43.21")),
      m_groupAddress6(QStringLiteral("ff12::2115"))
{
    m_udpSocket4Send.bind(QHostAddress(QHostAddress::AnyIPv4), 0);
    m_udpSocket6Send.bind(QHostAddress(QHostAddress::AnyIPv6), m_udpSocket4Send.localPort());
    m_udpSocket4Send.setSocketOption(QAbstractSocket::MulticastTtlOption, 1);

    m_udpSocket4Rec.bind(QHostAddress::AnyIPv4, UDP_PORT, QUdpSocket::ShareAddress);
    m_udpSocket4Rec.joinMulticastGroup(m_groupAddress4);
    connect(&m_udpSocket4Rec,SIGNAL(readyRead()), this,SLOT(readMessage()));


    if(m_udpSocket6Rec.bind(QHostAddress::AnyIPv6, UDP_PORT, QUdpSocket::ShareAddress)) {
        m_udpSocket6Rec.joinMulticastGroup(m_groupAddress6);
        connect(&m_udpSocket6Rec,SIGNAL(readyRead()), this,SLOT(readMessage()));
    }

    m_Timer = new QTimer(this);
    m_Timer->setInterval(5000);
    m_Timer->setSingleShot(false);
    connect(m_Timer,SIGNAL(timeout()), this,SLOT(sendGreeting()));
    m_Timer->start();
}

MessageManager::~MessageManager()
{
    QSettings getName;
    sendRawMessage(EVERYBODY_STRING + DEST_SEPERATOR + FAREWELL + END_MESSAGE_SEPERATOR +
                   getName.value("hostName").toString());
}



void MessageManager::sendCustomMessage(QString getMessage, QString getWhom)
{
    QSettings getName;
    sendRawMessage(getWhom + DEST_SEPERATOR + getMessage + END_MESSAGE_SEPERATOR +
                   getName.value("hostName").toString());
}

void MessageManager::sendGreeting()
{
    QSettings getName;
    sendRawMessage(EVERYBODY_STRING + DEST_SEPERATOR + GREETING + END_MESSAGE_SEPERATOR +
                   getName.value("hostName").toString());
}

void MessageManager::sendHelpRequest(QString toWhom)
{
    QSettings getName;
    sendRawMessage(EVERYBODY_STRING + DEST_SEPERATOR + NEED_HELP + END_MESSAGE_SEPERATOR +
                   getName.value("hostName").toString());
}

QStringList MessageManager::getHostNames()
{
    return m_HostNames;
}

void MessageManager::readMessage()
{
    QByteArray datagram;
    QHostAddress sender;
    quint16 senderPort;

    while (m_udpSocket4Rec.hasPendingDatagrams()) {
        datagram.resize(qsizetype(m_udpSocket4Rec.pendingDatagramSize()));
        m_udpSocket4Rec.readDatagram(datagram.data(), datagram.size(),
                                 &sender, &senderPort);
        processUDPData(datagram);
    }

    while (m_udpSocket6Send.hasPendingDatagrams()) {
        datagram.resize(qsizetype(m_udpSocket6Send.pendingDatagramSize()));
        m_udpSocket6Send.readDatagram(datagram.data(), datagram.size(),
                                 &sender, &senderPort);
        processUDPData(datagram);
    }
}

void MessageManager::processUDPData(QByteArray getData)
{
    QString sData = getData.data();
    QSettings getName;
    QString meString = getName.value("hostName").toString();

    qDebug()<<sData;

    if(sData.startsWith(EVERYBODY_STRING) || sData.startsWith(meString) ) {
    //if(true) {
        if(sData.startsWith(EVERYBODY_STRING)) {
            sData = sData.replace(EVERYBODY_STRING + DEST_SEPERATOR,"");
        }
        else {
            sData = sData.replace(meString + DEST_SEPERATOR,"");
        }
        //now sData = ". | From: [Source]"
        QStringList messageParts = sData.split(END_MESSAGE_SEPERATOR);
        //qDebug()<<messageParts;
        //[0] = [GREETING/FAREWELL/NEED_HELP/Custom]
        //[1] = [source]

        if(messageParts[1] == meString) {
            return; //ignore messages that I send
        }

        if(messageParts[0].startsWith(GREETING)) {
            QString newFriend = messageParts[1];
            if(!m_HostNames.contains(newFriend)) {
                m_HostNames.append(newFriend);
                emit hostNamesUpdated();
            }
        }
        else if(messageParts[0].startsWith(FAREWELL)) {
            QString lostFriend = messageParts[1];
            int removeIndex = m_HostNames.indexOf(lostFriend);
            sData.removeAt(removeIndex);
            emit hostNamesUpdated();
        }
        else {
            emit gotNewMessage(messageParts[0], messageParts[1]);
        }
    }

}

void MessageManager::sendRawMessage(QString getMessage)
{
    m_udpSocket4Send.writeDatagram(getMessage.toUtf8(), m_groupAddress4, UDP_PORT);
    m_udpSocket6Send.writeDatagram(getMessage.toUtf8(), m_groupAddress6, UDP_PORT);
}

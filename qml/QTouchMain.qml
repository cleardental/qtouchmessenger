//QTouchMessenger: QML based message app
//Copyright (C) 2024  Dr. Tej A. Shah

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtCore
import QtQuick.Controls
import QtQuick.VirtualKeyboard
import QtQuick.Layouts
import clear.dental
import Qt.labs.platform
import QtMultimedia

ApplicationWindow {
    id: rootWin
    width: 480
    height: 854
    visible: false
    title: qsTr("Touch Messenger")
    property alias globalSettings: appSettings
    property alias globalDrawer: settingsDrawer
    property alias globalMM: mMan
    property alias globalPalette: myPalette
    property string whoToSendTo:"Everybody"
    property bool killMe: false

    SystemPalette {
        id: myPalette;
        colorGroup: SystemPalette.Active
    }


    onClosing: (close) => {
                   close.accepted = killMe
                   rootWin.visible = false;
               }

    function pushToMessage() {
        mainStack.push("SelectMessagePage.qml");
    }

    function popBack() {
        mainStack.pop();
    }


    Settings {
        id: appSettings
    }

    MessageManager {
        id: mMan
        onGotNewMessage: {
            sysTray.showMessage(
                        qsTr("New Message from: ") + fromWho,
                        newMessage,
                        SystemTrayIcon.Information,
                        alertTimeBox.value * 1000);
            playSound.play();
        }
    }

    header: ToolBar {
            background: Rectangle{
                color: rootWin.globalPalette.highlight
            }
            RowLayout {
                anchors.fill: parent

                ToolButton {
                    icon.name: "arrow-left"
                    visible: mainStack.depth > 1
                    onClicked: {
                        mainStack.pop();
                    }
                }


                ToolButton {
                    icon.name: "application-menu"
                    onClicked: {
                        rootWin.globalDrawer.open();
                    }
                }

                Label {
                    id: headerTitleLabel
                    font.bold: true
                    text: mainStack.depth == 1 ? qsTr("Select who to send the message to") :
                                                 qsTr("Select message to send to ") + rootWin.whoToSendTo

                    elide: Label.ElideRight
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                }
            }
        }

    StackView {
        id: mainStack
        anchors.fill: parent
        initialItem: "SelectDestPage.qml"
    }

    Drawer {
        id: settingsDrawer
        width: settingsGrid.implicitWidth + 10
        height: rootWin.height
        GridLayout {
            id: settingsGrid
            columns: 2
            anchors.left: parent.left
            anchors.margins: 5

            DescLabel {
                text: qsTr("Local Name")
            }
            TextField {
                id: localNameField
                text: appSettings.value("hostName")
                onTextEdited: {
                    appSettings.setValue("hostName",localNameField.text)
                }
            }
            DescLabel {
                text: qsTr("Message Alert Time")
            }
            SpinBox {
                id: alertTimeBox
                value: appSettings.value("alertTime",10)
                onValueChanged: appSettings.setValue("alertTime",alertTimeBox.value)
                textFromValue: function(convertValue) {
                    return qsTr("%1 seconds").arg(convertValue)
                }
            }
            Button {
                text: "Kill App"
                onClicked: {
                    killMe = true;
                    Qt.quit();
                }
            }
        }
    }

    SystemTrayIcon {
        id: sysTray
        visible: true
        icon.source: "qrc:/icons/clearDental-instantMessage.svg"
        onActivated: {
            rootWin.visible = true;
        }
        menu: Menu {
            MenuItem {
                text: "Kill App"
                onTriggered: {killMe = true;Qt.quit();}
            }
        }
    }

    Shortcut {
        sequence: "Esc"
        onActivated: {
            sysTray.showMessage(qsTr("Application is Minimized"),
                                qsTr("The application is still running"),
                                SystemTrayIcon.Information,
                                3000);
            rootWin.visible = false;
        }
    }

    Shortcut {
        sequence: "Ctrl+Q"
        onActivated:{killMe = true; Qt.quit();}
    }

    MediaPlayer {
        id: playSound
        source: "qrc:/sounds/message-new-instant.oga"
        audioOutput: AudioOutput {
            volume: 1
        }
        loops: 5
    }

    InputPanel {
        id: inputPanel
        z: 99
        parent: settingsDrawer.visible ? settingsDrawer.contentItem : rootWin.contentItem

        x: 0
        y: rootWin.height
        width: rootWin.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: parent.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

}

//QTouchMessenger: QML based message app
//Copyright (C) 2024  Dr. Tej A. Shah

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtCore
import QtQuick.Controls
import QtQuick.VirtualKeyboard
import QtQuick.Layouts
import clear.dental

Page {

    ListView {
        anchors.fill: parent
        anchors.margins: 10
        model: [qsTr("Need Assistant"),
            qsTr("Need Doctor"),
            qsTr("Need Front Desk"),
            qsTr("I'm going to be running late"),
            qsTr("Please seat the next patient"),
        ]

        ButtonGroup {
            id: radiobuttonGroup
        }

        delegate: RowLayout {
            RadioButton {
                text: modelData
                Component.onCompleted: {
                    radiobuttonGroup.addButton(this);
                    if(text === qsTr("Need Assistant")) {
                        checked = true;
                    }
                }
            }
        }


        footer:ColumnLayout {
            RowLayout {
                RadioButton {
                    id: getMeSomething
                    text: qsTr("I need somebody to get me:")
                    Component.onCompleted: {
                        radiobuttonGroup.addButton(this);
                    }

                }
                ColumnLayout {
                    id: getMeRadioCol
                    visible: getMeSomething.checked
                    RadioButton {
                        id: basicKitButton
                        text: qsTr("Basic Kit")
                        checked: true
                    }
                    RadioButton {
                        id: radiographHodlerButton
                        text: qsTr("Radiograph holder")
                    }
                    RadioButton {
                        id: radiographSensorButton
                        text: qsTr("Radiograph sensor")
                    }
                    ButtonGroup {
                        id: getMeSomethingGroup
                        buttons: [basicKitButton,radiographHodlerButton,radiographSensorButton]
                    }
                }
            }
            RowLayout {
                RadioButton {
                    id: somethingElseButton
                    text: qsTr("Something else")
                    Component.onCompleted: {
                        radiobuttonGroup.addButton(this);
                    }
                    onCheckedChanged: {
                        if(checked) {
                            somethingElseField.focus = true;
                        }
                    }
                }
                TextField {
                    id: somethingElseField
                    enabled: somethingElseButton.checked
                }
            }

            Button {
                text: qsTr("Send")
                onClicked: {
                    var messageText = radiobuttonGroup.checkedButton.text;
                    if(getMeSomething.checked) {
                        messageText = getMeSomething.text +" " + getMeSomethingGroup.checkedButton.text;
                    }
                    else if(somethingElseButton.checked) {
                        messageText = somethingElseField.text
                    }

                    rootWin.globalMM.sendCustomMessage(messageText,rootWin.whoToSendTo);
                    doneDia.open();
                }
            }
        }
    }

    Dialog {
        id: doneDia
        title: qsTr("Message Sent")
        anchors.centerIn: parent
        modal: true
        closePolicy: Popup.NoAutoClose
        onVisibleChanged: {
            if(visible) {
                closeTimer.start();
            }
        }

        Timer {
            id: closeTimer
            interval: 2000;
            onTriggered:  {
                doneDia.close();
                rootWin.popBack();
            }

        }
    }
}
